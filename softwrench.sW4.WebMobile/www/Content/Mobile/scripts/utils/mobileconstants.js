﻿(function (window) {
    "use strict";

    window.constants = window.constants || {};
    window.constants.localIdKey = "#localswdbid";
    window.constants.isDirty = "#localisDirty";
    window.constants.isPending = "#localisPending";
    window.constants.newItem = "_newitem#$";

})(window);
