﻿(function (mobileServices) {
    "use strict";

    mobileServices.constant("crudConstants", {
        operation: {
            'create': "crud_create",
            'update': "crud_update"
        }
    });


})(mobileServices);