﻿class ConfigurationKeys {
    static get FacilitiesChanged() {
        return "facilitieschanged";
    }

    static get ServerConfig() {
        return "serverconfig";
    }
}