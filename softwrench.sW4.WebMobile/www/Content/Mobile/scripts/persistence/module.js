﻿(function (angular) {
    "use strict";

    angular.module("persistence.offline", []);

})(angular);