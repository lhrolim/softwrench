﻿namespace softwrench.sw4.Hapag.Data {
    public enum RoleType {
        Incident,
        Asset,
        ITCSearch,
        Faq,
        NewImac,
        ImacGrid,
        TapeBackupReport,
        HardwareRepairReport,
        IncidentDetailsReport,
        IncidentPerLocationReport,
        AssetCategoriesReport,
        ITCReport
    }
}
