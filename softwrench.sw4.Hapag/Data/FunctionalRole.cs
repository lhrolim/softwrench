﻿namespace softwrench.sw4.Hapag.Data {
    public enum FunctionalRole {
        AssetControl,
        AssetRamControl,
        XItc,
        Purchase,
        Tom,
        Itom,
        Ad,
        Change,
        Sso,
        Tui
    }
}