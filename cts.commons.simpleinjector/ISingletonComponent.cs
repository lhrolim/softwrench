﻿namespace cts.commons.simpleinjector {
    /// <summary>
    /// Implement this interface if you need A SINGLE bean implementing a service Interface/Superclass.
    /// </summary>
    public interface ISingletonComponent : IComponent { }
}