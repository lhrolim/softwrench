﻿namespace cts.commons.simpleinjector {
    /// <summary>
    /// Implement this interface if you need MULTIPLE BEANS implementing the same service Interface/Superclass.
    /// </summary>
    public interface IComponent { }
}