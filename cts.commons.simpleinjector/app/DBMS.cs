﻿namespace cts.commons.persistence {
    public enum DBMS {
        MSSQL, DB2, ORACLE, MYSQL
    }
}