﻿namespace cts.commons.simpleinjector.Core.Order {

    public interface IOrdered
    {
        int Order { get; }
    }
}