﻿using System;

namespace softwrench.sw4.webcommons.classes.api {
    [AttributeUsage(AttributeTargets.Class)]
    public class RawController : Attribute {
    }
}