﻿namespace softwrench.sw4.batch.api.entities {
    public enum BatchStatus {
        INPROG, SUBMITTING, COMPLETE, COMPLETE_WITH_PROBLEMS
    }
}