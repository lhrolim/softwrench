﻿using NHibernate.Type;

namespace softwrench.sw4.batch.api.entities {
    class BatchStatusType : EnumStringType<BatchStatus> {
    }
}
