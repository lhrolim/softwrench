﻿namespace softwrench.sw4.Shared2.Metadata.Applications.Filter {

    public class MetadataNumberFilter : BaseMetadataFilter {

        public MetadataNumberFilter(string attribute, string label, string icon, string position, string tooltip, string whereClause) : base(attribute, label, icon, position, tooltip, whereClause) {
        }
    }
}
