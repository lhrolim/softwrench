﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace softwrench.sw4.Shared2.Metadata.Applications.Schema {
    public class SchemaRepresentation {

        public string Label { get; set; }
        public string SchemaId { get; set; }

    }
}
