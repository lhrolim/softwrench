﻿namespace softwrench.sW4.Shared2.Metadata.Applications.Schema {
    public enum SchemaStereotype {
        List, Detail,DetailNew, CompositionList, CompositionDetail, Notification, None, Search
    }
}