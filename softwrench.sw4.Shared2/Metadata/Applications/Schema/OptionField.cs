﻿using cts.commons.portable.Util;
using softwrench.sw4.Shared2.Data.Association;
using softwrench.sw4.Shared2.Metadata;
using softwrench.sw4.Shared2.Metadata.Applications.Schema;
using softwrench.sw4.Shared2.Metadata.Applications.Schema.Interfaces;
using softwrench.sw4.Shared2.Metadata.Applications.UI;
using softwrench.sW4.Shared2.Metadata.Applications.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using softwrench.sw4.Shared2.Metadata.Applications.Relationships.Associations;

namespace softwrench.sW4.Shared2.Metadata.Applications.Schema {

    /// <summary>
    /// Type of Field which is obrigatory a combo with a fixed list of options (AssociationOption).<p/> 
    /// 
    /// It´s represented by the <optionfield></optionfield> tag in the metadata.xml
    /// 
    /// </summary>
    public class OptionField : BaseApplicationFieldDefinition, IDataProviderContainer, IDependableField, IPCLCloneable, IExtraProjectionProvider {

        private readonly FieldFilter _filter;
        private readonly string _extraParameter;
        private readonly ISet<string> _dependantFields = new HashSet<string>();
        private readonly bool _sort;
        private readonly ISet<ApplicationEvent> _eventsSet;
        private readonly string _dependantFieldsString;
        public List<IAssociationOption> Options { get; set; }

        public string EvalExpression {
            get; set;
        }

        public OptionField() {

        }

        public OptionField(string applicationName, string label, string attribute, string qualifier, string requiredExpression, bool isReadOnly, bool isHidden,
            OptionFieldRenderer renderer, FieldFilter filter, List<IAssociationOption> options, string defaultValue, bool sort, string showExpression, string helpIcon,
            string toolTip, string attributeToServer, ISet<ApplicationEvent> events, string providerAttribute, string dependantFields, string enableExpression,
            string evalExpression, string extraParameter, string defaultExpression, string searchOperation, string extraProjectionFields = null)
            : base(applicationName, label, attribute, requiredExpression, isReadOnly, defaultValue, qualifier, showExpression, helpIcon, toolTip, attributeToServer, events, enableExpression, defaultExpression, false, searchOperation) {
            Renderer = renderer;
            RendererParameters = Renderer == null ? new Dictionary<string, object>() : Renderer.ParametersAsDictionary();
            RendererType = Renderer?.RendererType.ToLower();
            _filter = filter;
            Options = options;
            ProviderAttribute = providerAttribute;
            _extraParameter = extraParameter;
            IsHidden = isHidden;
            _sort = sort;
            if (sort) {
                Options?.Sort();
            }
            _eventsSet = events;
            _dependantFieldsString = dependantFields;
            _dependantFields = ParsingUtil.GetCommaSeparetedParsingResults(dependantFields);
            ExtraProjectionFields = ExtraProjectionProviderHelper.BuildExtraProjectionFields(extraProjectionFields);
            EnableExpression = enableExpression;
            EvalExpression = evalExpression;
        }

        public override bool IsHidden {
            get; set;
        }

        public OptionFieldRenderer Renderer { get; set; }

        public IDictionary<string, object> RendererParameters { get; set; }

        public override string RendererType { get; set; }

        public FieldFilter Filter => _filter;

        public IDictionary<string, object> FilterParameters => _filter == null ? new Dictionary<string, object>() : _filter.ParametersAsDictionary();


        public string AssociationKey => ProviderAttribute ?? Attribute;

        public string Target => Attribute;

        public string ProviderAttribute { get; set; }

        public string ExtraParameter => _extraParameter;



        public ISet<string> DependantFields => _dependantFields;

        public bool Sort => _sort;

        public string ApplicationPath => AssociationKey;

        public ISet<string> ExtraProjectionFields {
            get; set;
        }

        public object Clone() {
            var optionField = new OptionField(ApplicationName, Label, Attribute, Qualifier, RequiredExpression,
                IsReadOnly, IsHidden, Renderer, _filter,
                Options,
                DefaultValue, _sort, ShowExpression, HelpIcon, ToolTip, AttributeToServer, _eventsSet, ProviderAttribute,
                _dependantFieldsString, EnableExpression, EvalExpression, _extraParameter, DefaultExpression,
                SearchOperation) {
                ExtraProjectionFields = ExtraProjectionFields
            };
            return optionField;
        }

        public override string ToString() {
            return string.Format("ProviderAttribute: {0}, Target: {1}", ProviderAttribute, Target);
        }
    }
}
