﻿using System;

namespace softwrench.sW4.Shared2.Metadata.Applications.Schema.Interfaces
{
    public interface IApplicationIndentifiedDisplayable :IApplicationDisplayable
    {
        String Attribute { get; set; }
     

    }
}