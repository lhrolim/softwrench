﻿using System;
using System.Collections.Generic;
using softwrench.sW4.Shared2.Metadata.Applications.Schema.Interfaces;
using softwrench.sw4.Shared2.Metadata.Applications.Schema.Interfaces;
using softwrench.sW4.Shared2.Metadata.Applications.Relationships.Compositions;

namespace softwrench.sw4.Shared2.Metadata.Applications.UI {
    public class ApplicationTabDefinition : IApplicationIndentifiedDisplayable, IApplicationIdentifier, IApplicationDisplayableContainer {

        public string Id {
            get; set;
        }
        public string ApplicationName {
            get; set;
        }
        public string Label {
            get; set;
        }
        public string Icon {
            get; set;
        }
        private List<IApplicationDisplayable> _displayables = new List<IApplicationDisplayable>();

        /// <summary>
        /// If true an extra dataset method will be invoked to load any related data which this tab needs
        /// </summary>
        public bool Lazy { get; set; }

        private string _role;

        public ApplicationTabDefinition(string id, string applicationName, string label, List<IApplicationDisplayable> displayables, string toolTip,
            string showExpression, string enableExpression, string icon, string role, string countRelathionship, bool lazy) {
            Id = id;
            ApplicationName = applicationName;
            Label = label;
            ToolTip = toolTip;
            ShowExpression = showExpression;
            EnableExpression = enableExpression;
            _displayables = displayables;
            Icon = icon;
            _role = role;
            Lazy = lazy;
            CountRelationship = countRelathionship;
        }


        public string RendererType => null;

        public string Type => typeof(ApplicationTabDefinition).Name;

        public string Role {
            get {
                if (_role != null) {
                    return _role;
                }
                return ApplicationName + "." + Id;
            }
            set {
                _role = value;
            }
        }

        public string ShowExpression {
            get; set;
        }

        public string EnableExpression {
            get; set;
        }

        public string ToolTip {
            get; set;
        }
        public string IdFieldName {
            get; set;
        }
        public bool IsReadOnly {
            get {
                return false;
            }
            set {
            }
        }
        public string TabId => Id;

        public string CountRelationship {
            get; set;
        }

        public List<IApplicationDisplayable> Displayables {
            get {
                return _displayables;
            }
            set {
                _displayables = value;
            }
        }

        public string Attribute {
            get {
                return TabId;
            }
            set {
            }
        }

        public override string ToString() {
            return TabId;
        }

        public ApplicationTabDefinition Clone() {
            var cloned = new ApplicationTabDefinition(Id,ApplicationName,Label,Displayables,ToolTip,ShowExpression,EnableExpression,Icon,Role,CountRelationship,Lazy);
            return cloned;

        }

    }
}
