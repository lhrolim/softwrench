﻿using NHibernate.Type;
using softwrench.sW4.Shared2.Metadata.Applications;

namespace softwrench.sW4.Shared2.Metadata.Applications {
    public class ClientPlatformType :  EnumStringType<ClientPlatform> {
    }
}