﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using softwrench.sw4.Shared2.Metadata.Applications.Schema;

namespace softwrench.sw4.Shared2.Metadata {
    public class SharedApplicationData {
        public IEnumerable<OptionContainer> Options { get; set; } 

    }
}
