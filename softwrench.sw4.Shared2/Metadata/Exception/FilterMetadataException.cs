﻿using System;
using System.Runtime.Serialization;

namespace softwrench.sw4.Shared2.Metadata.Exception {

    public class FilterMetadataException : MetadataException {


        public FilterMetadataException(string message) : base(message) { }

     
    }
}
