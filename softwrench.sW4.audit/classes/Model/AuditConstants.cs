﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace softwrench.sW4.audit.classes.Model {

    public class AuditConstants {
        public const string AuditEnabled = "/Global/Audit/Enabled";
        
        public const string AuditQueryEnabled = "/Global/Audit/AuditQueryEnabled";
    }
}
