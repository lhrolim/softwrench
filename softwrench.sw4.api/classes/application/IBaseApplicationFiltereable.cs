﻿namespace softwrench.sw4.api.classes.application
{
    public interface IBaseApplicationFiltereable
    {
        string ApplicationName();
        string ClientFilter();
    }
}