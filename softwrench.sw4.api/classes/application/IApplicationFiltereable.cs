﻿namespace softwrench.sw4.api.classes.application {
    public interface IApplicationFiltereable : IBaseApplicationFiltereable {

        string SchemaId();
    }
}