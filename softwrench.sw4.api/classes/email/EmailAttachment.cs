﻿namespace softwrench.sw4.api.classes.email {

    public class EmailAttachment {
        public string AttachmentData { get; set; }
        public string AttachmentName { get; set; }
        public byte[] AttachmentBinary { get; set; }
    }
}
