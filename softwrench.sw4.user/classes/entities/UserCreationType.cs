﻿namespace softwrench.sw4.user.classes.entities {
    public enum UserCreationType {
        Integration, Self, Admin
    }
}