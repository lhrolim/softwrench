﻿namespace softwrench.sw4.user.classes.entities.security
{
    public enum SchemaPermissionMode
    {
        Grid, Creation, Update, View
    }
}