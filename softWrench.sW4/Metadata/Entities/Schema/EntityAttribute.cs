﻿using cts.commons.portable.Util;
using JetBrains.Annotations;
using softWrench.sW4.Metadata.Entities.Connectors;
using softWrench.sW4.Util;
using System;
using cts.commons.Util;
using softwrench.sw4.Shared2.Metadata.Entity;

namespace softWrench.sW4.Metadata.Entities.Schema {
    public class EntityAttribute : IQueryHolder {
        public const string AttributeQualifierSeparator = ".";

        public EntityAttribute([NotNull] string name, [NotNull] string type, bool requiredExpression, bool isAutoGenerated, [NotNull] ConnectorParameters connectorParameters, string query) {
            Validate.NotNull(name, "name");
            Validate.NotNull(type, "type");
            Validate.NotNull(connectorParameters, "connectorParameters");

            Name = name;
            Type = type;
            Query = query;
            RequiredExpression = requiredExpression;
            ConnectorParameters = connectorParameters;
            IsAutoGenerated = isAutoGenerated;
        }

        [NotNull]
        public string Name { get; }

        [NotNull]
        public string Type { get; }


        public bool RequiredExpression { get; }

        public string Query {
            get; set;
        }

        [NotNull]
        public ConnectorParameters ConnectorParameters { get; }

        public bool IsAutoGenerated { get; }

        public bool IsAssociated => Name.Contains(AttributeQualifierSeparator);

        public bool IsDate => Type == "timestamp" || Type == "datetime";

        public bool IsNumber => Type.EqualsAny("int", "bigint", "float", "integer", "decimal", "double");

        public override string ToString() {
            return $"Name: {Name}, Type: {Type}";
        }

        protected bool Equals(EntityAttribute other) {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((EntityAttribute)obj);
        }

        public override int GetHashCode() {
            return (Name.GetHashCode());
        }

        public virtual string GetQueryReplacingMarkers(string entityName, string fromValue = null, string context = null) {
            if (Query.StartsWith("ref:")) {
                Query = entityName.StartsWith("#") ? MetadataProvider.SwdbEntityQuery(Query) : MetadataProvider.EntityQuery(Query);
            }
            return EntityUtil.GetQueryReplacingMarkers(Query, entityName, fromValue, context);
        }

        public EntityAttribute ClonePrependingContext(string context) {
            if (context == null) {
                return this;
            }

            var finalName = Name.Contains(".") ? context + Name : context + "." + Name;

            return new ContextualEntityAttribute(finalName, Type, RequiredExpression,
                IsAutoGenerated, ConnectorParameters, Query, context);
        }
    }
}