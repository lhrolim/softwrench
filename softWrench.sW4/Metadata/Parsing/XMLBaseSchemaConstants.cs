﻿namespace softWrench.sW4.Metadata.Parsing {
    public class XmlBaseSchemaConstants {
        public const string IdAttribute = "id";
        public const string StereotypeAttribute = "stereotype";

        public const string ServiceAttribute = "service";
        public const string MethodAttribute = "method";
        public const string RootElement = "metadata";
        public const string BaseDisplayableShowExpressionAttribute = "showexpression";
        public const string BaseDisplayableEnableExpressionAttribute = "enableexpression";
        public const string BaseDisplayableEvalExpressionAttribute = "evalexpression";
        public const string BaseDisplayableEnableDefaultAttribute = "enabledefault";
        public const string BaseDisplayableToolTipAttribute = "tooltip";
        public const string BaseDisplayableHelpIconAttribute = "helpicon";
        public const string BaseDisplayableHiddenAttribute = "hidden";
        public const string BaseDisplayablePrintEnabledAttribute = "printenabled";
        public const string BaseDisplayableAttributeAttribute = "attribute";
        public const string BaseDisplayableLabelAttribute = "label";
        public const string BaseDisplayableIdAttribute = "id";
        public const string BaseDisplayableRequiredExpressionAttribute = "requiredexpression";
        public const string BaseDisplayableSearchOperation = "searchoperation";
        public const string BaseParametersAttribute = "parameters";
        public const string BasePropertiesAttribute = "properties";
        public const string IconAttribute = "icon";
        public const string BaseDisplayableQueryAttribute = "query";
        public const string CssClassesAttribute = "cssclasses";
        public const string PrimaryAttribute = "primary";

        public const string LabelAttribute = "label";
        public const string ValueAttribute = "value";
        public const string PreSelectedAttribute = "preselected";
        public const string TooltipAttribute = "tooltip";
        public const string DisplayCodeAttribute = "displaycode";
        public const string RoleAttribute = "role";
        public const string CountRelationshipAttribute = "countrelationship";
        public const string HasLazyDataAttribute = "haslazydata";

        public const string ToggleButtonInitialStateExpressionAttribute = "initialstateexpression";
    }
}
