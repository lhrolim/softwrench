﻿namespace softWrench.sW4.Metadata.Security {

    public class SecurityApplicationEntry {

        public string Id { get; set; }
        public string Ref { get; set; }
        public string Label { get; set; }
        public string ListSchema { get; set; }
        public string CreationSchema { get; set; }
        public string UpdateSchema { get; set; }

    }
}
