﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace softWrench.sW4.Metadata {
    
    /// <summary>
    /// Indicates a metadata refresh.
    /// </summary>
    public class RefreshMetadataEvent {
    }
}
