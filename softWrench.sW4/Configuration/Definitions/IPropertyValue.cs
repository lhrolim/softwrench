﻿namespace softWrench.sW4.Configuration.Definitions
{
    public interface IPropertyValue
    {
        string StringValue { get; set; }
        string SystemStringValue { get; set; }
    }
}