﻿namespace softWrench.sW4.Data.Persistence.WS.Internal.Constants {
    public enum WsProvider {
        // ReSharper disable InconsistentNaming
        MIF, MEA, ISM, REST
        // ReSharper restore InconsistentNaming
    }
}