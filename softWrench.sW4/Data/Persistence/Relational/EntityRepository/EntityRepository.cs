﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cts.commons.persistence;
using cts.commons.portable.Util;
using JetBrains.Annotations;
using log4net;
using softWrench.sW4.Data.Offline;
using softWrench.sW4.Data.Search;
using softWrench.sW4.Metadata.Entities;
using softWrench.sW4.Metadata.Entities.Sliced;
using softwrench.sW4.Shared2.Data;
using Quartz.Util;
using softWrench.sW4.Data.Pagination;
using softWrench.sW4.Util;

namespace softWrench.sW4.Data.Persistence.Relational.EntityRepository {
    public class EntityRepository : IEntityRepository {

        private static readonly ILog Log = LogManager.GetLogger(typeof(EntityRepository));

        private readonly ISWDBHibernateDAO _swdbDao;
        private readonly IMaximoHibernateDAO _maximoHibernateDao;
        private readonly EntityQueryBuilder _entityQueryBuilder;

        public EntityRepository(ISWDBHibernateDAO swdbDao, IMaximoHibernateDAO maximoHibernateDao) {
            _swdbDao = swdbDao;
            _maximoHibernateDao = maximoHibernateDao;
            _entityQueryBuilder = new EntityQueryBuilder();
        }


        private static string InitializeConnectionString() {
            return ApplicationConfiguration.DBConnectionString(DBType.Maximo);
        }

        public async Task<IReadOnlyList<DataMap>> Get(EntityMetadata entityMetadata, SearchRequestDto searchDto) {
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));
            if (searchDto == null) throw new ArgumentNullException(nameof(searchDto));
            var query = _entityQueryBuilder.AllRows(entityMetadata, searchDto);
            var rows = await Query(entityMetadata, query, searchDto);
            return rows.Cast<IEnumerable<KeyValuePair<string, object>>>()
               .Select(r => BuildDataMap(entityMetadata, r, entityMetadata.IdFieldName))
               .ToList();

        }

        public async Task<IDictionary<string, DataMap>> GetGrouppingById([NotNull] EntityMetadata entityMetadata, [NotNull] SearchRequestDto searchDto) {
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));
            if (searchDto == null) throw new ArgumentNullException(nameof(searchDto));
            var query = _entityQueryBuilder.AllRows(entityMetadata, searchDto);
            var rows = await Query(entityMetadata, query, searchDto);
            var castedRows = rows.Cast<IEnumerable<KeyValuePair<string, object>>>();
            var results = new Dictionary<string, DataMap>();
            foreach (var row in castedRows) {
                var dm = BuildDataMap(entityMetadata, row, entityMetadata.IdFieldName);
                results.Add(dm.Id, dm);
            }
            return results;

        }

        /// <summary>
        /// 'Deprecated' method, however there´s a bug on NhibernateX whereas some invalid queries crash the app without any exception being thrown.
        /// Using this method to validate whereclauses input by the users.
        /// </summary>
        /// <param name="entityMetadata"></param>
        /// <param name="searchDto"></param>
        /// <returns></returns>
        public IReadOnlyList<DataMap> GetSync([NotNull] EntityMetadata entityMetadata, SearchRequestDto searchDto = null) {
            if (entityMetadata == null) {
                throw new ArgumentNullException(nameof(entityMetadata));
            }
            if (searchDto == null) {
                searchDto = new SearchRequestDto();
            }
            var query = _entityQueryBuilder.AllRows(entityMetadata, searchDto);
            var rows = QuerySync(entityMetadata, query, searchDto);
            return rows.Cast<IEnumerable<KeyValuePair<string, object>>>()
               .Select(r => BuildDataMap(entityMetadata, r, entityMetadata.IdFieldName))
               .ToList();

        }



        public DataMap BuildDataMap(EntityMetadata entityMetadata, IEnumerable<KeyValuePair<string, object>> r, string idFieldName = null) {


            return new DataMap(entityMetadata.Name, r.ToDictionary(pair => FixKey(pair.Key, entityMetadata), pair => HandleValue(pair.Key, entityMetadata, pair.Value), StringComparer.OrdinalIgnoreCase), entityMetadata.Schema.MappingType, false, idFieldName);
        }

        private object HandleValue(string key, EntityMetadata entityMetadata, object value) {
            if (!(value is DateTime)) {
                return value;
            }
            var attributeDeclaration = entityMetadata.Schema.Attributes.FirstOrDefault(f => f.Name.EqualsIc(key));
            if (attributeDeclaration == null) {
                return value;
            }
            if (attributeDeclaration.ConnectorParameters.Parameters.ContainsKey("utcdate")) {
                var date = (DateTime)value;
                date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
                return date;
            }
            if (attributeDeclaration.ConnectorParameters.Parameters.ContainsKey("offset")) {
                var date = (DateTime)value;
                return new DateTimeOffset(date);
            }
            if (entityMetadata.SWEntity()) {
                var date = (DateTime)value;
                date = DateTime.SpecifyKind(date, DateTimeKind.Local);
                return date;
            }
            return value;
        }

        public async Task<IEnumerable<dynamic>> RawGet([NotNull] EntityMetadata entityMetadata, [NotNull] SearchRequestDto searchDto) {
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));
            if (searchDto == null) throw new ArgumentNullException(nameof(searchDto));
            var query = _entityQueryBuilder.AllRows(entityMetadata, searchDto);
            var rows = await Query(entityMetadata, query, searchDto);
            return rows;
        }

        public async Task<IReadOnlyList<DataMap>> Get([NotNull] EntityMetadata entityMetadata, long rowstamp, SearchRequestDto searchDto = null) {
            if (entityMetadata == null) {
                throw new ArgumentNullException(nameof(entityMetadata));
            }
            if (searchDto == null) {
                searchDto = new SearchRequestDto();
            }
            var query = _entityQueryBuilder.AllRows(entityMetadata, searchDto);
            var rows = await Query(entityMetadata, query, rowstamp, searchDto);
            return rows.Cast<IEnumerable<KeyValuePair<string, object>>>()
               .Select(r => BuildDataMap(entityMetadata, r))
               .ToList();

        }



        public virtual async Task<IReadOnlyList<DataMap>> GetIdAndSiteIdByUserId([NotNull] EntityMetadata entityMetadata, string userId) {
            if (entityMetadata == null) {
                throw new ArgumentNullException(nameof(entityMetadata));
            }

            var query = _entityQueryBuilder.IdAndSiteIdByUserId(entityMetadata, userId);
            var rows = await Query(entityMetadata, query, new SearchRequestDto());
            return rows.Cast<IEnumerable<KeyValuePair<string, object>>>()
               .Select(r => BuildDataMap(entityMetadata, r))
               .ToList();
        }

        public class SearchEntityResult {
            public IList<Dictionary<string, object>> ResultList;
            public long? MaxRowstampReturned;
            public string IdFieldName;
            public bool Timeout { get; set; }

            /// <summary>
            /// Holds the PaginationData for the result
            /// </summary>
            [CanBeNull]
            public PaginatedSearchRequestDto PaginationData {
                get; set;
            }
        }



        //needed to avoid "Fields" nesting in collectionData
        public async Task<SearchEntityResult> GetAsRawDictionary([NotNull] EntityMetadata entityMetadata, [NotNull] SearchRequestDto searchDto, Boolean fetchMaxRowstamp = false) {
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));
            if (searchDto == null) throw new ArgumentNullException(nameof(searchDto));
            var query = _entityQueryBuilder.AllRows(entityMetadata, searchDto);
            var rows = await Query(entityMetadata, query, searchDto);
            var enumerable = rows as dynamic[] ?? rows.ToArray();
            Log.DebugFormat("returning {0} rows", enumerable.Count());
            long? maxRowstamp = 0;

            IList<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

            foreach (var row in enumerable) {
                var dict = (IDictionary<string, object>)row;
                var item = new Dictionary<string, object>();
                if (fetchMaxRowstamp) {

                    if (dict.ContainsKey(RowStampUtil.RowstampColumnName)) {
                        var rowstamp = RowStampUtil.Convert(dict[RowStampUtil.RowstampColumnName]);
                        if (rowstamp > maxRowstamp) {
                            maxRowstamp = rowstamp;
                        }
                    }
                }

                foreach (var column in dict) {
                    var value = column.Value;
                    if (entityMetadata.IsSwDb && column.Value is DateTime) {
                        var date = (DateTime)column.Value;
                        value = DateTime.SpecifyKind(date, DateTimeKind.Local);
                    }
                    item[FixKey(column.Key, entityMetadata)] = value;


                }
                list.Add(item);
            }

            return new SearchEntityResult {
                MaxRowstampReturned = maxRowstamp == 0 ? null : maxRowstamp,
                ResultList = list,
                IdFieldName = entityMetadata.IdFieldName
            };



        }


        private async Task<IEnumerable<dynamic>> Query(EntityMetadata entityMetadata, BindedEntityQuery query, SearchRequestDto searchDTO) {
            if (searchDTO.ForceEmptyResult) {
                return Enumerable.Empty<dynamic>();
            }
            //TODO: hack to avoid garbage data and limit size of list queries.
            var paginationData = PaginationData.GetInstance(searchDTO, entityMetadata);
            var rows = await GetDao(entityMetadata).FindByNativeQueryAsync(query.Sql, query.Parameters, paginationData, searchDTO.QueryAlias);
            return rows;
        }

        private async Task<IEnumerable<dynamic>> Query(EntityMetadata entityMetadata, BindedEntityQuery query, long rowstamp, SearchRequestDto searchDto) {
            if (searchDto.ForceEmptyResult) {
                return Enumerable.Empty<dynamic>();
            }
            var sqlAux = query.Sql.Replace("1=1", RowStampUtil.RowstampWhereCondition(entityMetadata, rowstamp, searchDto));
            var rows = await GetDao(entityMetadata).FindByNativeQueryAsync(sqlAux, query.Parameters, null, searchDto.QueryAlias);
            return rows;
        }

        private IEnumerable<dynamic> QuerySync(EntityMetadata entityMetadata, BindedEntityQuery query, SearchRequestDto searchDto) {
            if (searchDto.ForceEmptyResult) {
                return Enumerable.Empty<dynamic>();
            }
            //TODO: hack to avoid garbage data and limit size of list queries.
            var paginationData = PaginationData.GetInstance(searchDto, entityMetadata);
            var rows = GetDao(entityMetadata).FindByNativeQuery(query.Sql, query.Parameters, paginationData, searchDto.QueryAlias);
            return rows;
        }

        //_ used in db2
        private string FixKey(string key, EntityMetadata entityMetadata) {
            if (entityMetadata.Attributes(EntityMetadata.AttributesMode.NoCollections).Any(a => a.Name.EqualsIc(key))) {
                //IF the entity has a non collection attribute declared containing _, let´s keep it, cause it could be the name of the column actually
                return key.ToLower();
            }

            // TODO: This needs to be revisited when we integrate any DB2 customer. 
            // TODO: Not working for current customer because they could have attributes that are with underscore like feature_request - KSW-104
            if (key.Contains("_") && !key.Contains(".")) {
                if (key.IndexOf("_", StringComparison.Ordinal) !=
                    key.LastIndexOf("_", StringComparison.Ordinal)) {
                    //more then one _ ==> replace only last
                    return key.ReplaceLastOccurrence("_", "_.").ToLower();
                }

                return key.Replace("_", "_.").ToLower();
            }
            return key.ToLower();
        }



        public async Task<DataMap> Get(EntityMetadata entityMetadata, string id) {
            //TODO: we're always handling the entity ID as a string.
            //Maybe we should leverage the entity attribute type.
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));
            if (id == null) throw new ArgumentNullException(nameof(id));

            var query = _entityQueryBuilder.ById(entityMetadata, id);


            var rows = await Query(entityMetadata, query, new SearchRequestDto());
            return rows.Cast<IEnumerable<KeyValuePair<string, object>>>()
              .Select(r => BuildDataMap(entityMetadata, r))
              .ToList().FirstOrDefault();
        }

        public async Task<AttributeHolder> ByUserIdSite(EntityMetadata entityMetadata, Tuple<string, string> userIdSiteTuple) {
            //TODO: we're always handling the entity ID as a string.
            //Maybe we should leverage the entity attribute type.
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));
            if (userIdSiteTuple == null) throw new ArgumentNullException(nameof(userIdSiteTuple));
            var query = _entityQueryBuilder.ByUserIdSite(entityMetadata, userIdSiteTuple);


            var rows = await Query(entityMetadata, query, new SearchRequestDto());
            return rows.Cast<IEnumerable<KeyValuePair<string, object>>>()
              .Select(r => BuildDataMap(entityMetadata, r))
              .ToList().FirstOrDefault();
        }

        public async Task<IList<IEnumerable<KeyValuePair<string, object>>>> GetSynchronizationData(SlicedEntityMetadata entityMetadata, Rowstamps rowstamps, SearchRequestDto searchDto) {

            var query = _entityQueryBuilder.AllRowsForSync(entityMetadata, rowstamps, searchDto);
            //TODO: hack to avoid garbage data and limit size of list queries.
            var sql = query.Sql;

            IPaginationData data = null;
            if (ApplicationConfiguration.IsOracle(entityMetadata.DbType) && entityMetadata.FetchLimit() != null) {
//                sql += " limit {0} ".Fmt(entityMetadata.FetchLimit());
                data = new PaginationData(entityMetadata.FetchLimit().Value, 1, null);
            }

            

            var queryResult = await GetDao(entityMetadata).FindByNativeQueryAsync(sql, query.Parameters, data, searchDto.QueryAlias);
            var rows = queryResult.Cast<IEnumerable<KeyValuePair<string, object>>>();
            return rows as IList<IEnumerable<KeyValuePair<string, object>>> ?? rows.ToList();

        }

        //     

        [NotNull]


        public async Task<int> Count(EntityMetadata entityMetadata, SearchRequestDto searchDto) {
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));
            if (searchDto == null) throw new ArgumentNullException(nameof(searchDto));
            if (searchDto.ForceEmptyResult) {
                return 0;
            }
            var query = _entityQueryBuilder.CountRows(entityMetadata, searchDto);

            return await GetDao(entityMetadata).CountByNativeQueryAsync(query.Sql, query.Parameters, searchDto.QueryAlias);

        }

        private IBaseHibernateDAO GetDao(EntityMetadata metadata) {
            if (metadata.Name.EndsWith("_")) {
                return _swdbDao;
            }
            return _maximoHibernateDao;
        }

        public static int GetNextEntityId([NotNull] EntityMetadata entityMetadata, string fieldName = null) {
            if (entityMetadata == null) throw new ArgumentNullException(nameof(entityMetadata));

            fieldName = fieldName ?? entityMetadata.IdFieldName;
            var query = "Select MAX({0}) from {1}".FormatInvariant(fieldName, entityMetadata.GetTableName());
            var id = MaximoHibernateDAO.GetInstance().FindSingleByNativeQuery<object>(query, null);
            var newId = Convert.ToInt32(id) + GetRandomIncrement();
            return newId;
        }

        public static int GetRandomIncrement() {
            var rnd = new Random();
            // To avoid concurrency issues with maximo we will use a range of 5-15 higher than the max.
            // Using the range also lowers the chance of two users in SW submitting new records at the same time and causing a similar issue.
            return rnd.Next(5, 15);
        }
    }
}
