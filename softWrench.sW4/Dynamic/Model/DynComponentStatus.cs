﻿namespace softWrench.sW4.Dynamic.Model {
    public enum DynComponentStatus {
        WillBeDeployed, Outdated, UpToDate, WillBeUndeployed, Undeployed
    }
}
