﻿namespace softwrench.sw4.problem.classes {
    public enum ProblemStatus {
        Open, Resolved
    }
}