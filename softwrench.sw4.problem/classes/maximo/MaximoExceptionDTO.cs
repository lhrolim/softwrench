﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace softwrench.sw4.problem.classes.maximo {
    public class MaximoExceptionDTO
    {

        public string ErrorCode { get; set; }

        public string FullMessage {get; set;}

        public string MainMessage {get; set;}

    }
}
