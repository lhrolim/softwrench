﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using cts.commons.persistence;
using cts.commons.portable.Util;
using cts.commons.simpleinjector;
using cts.commons.simpleinjector.Events;
using cts.commons.Util;
using Iesi.Collections.Generic;
using JetBrains.Annotations;
using log4net;
using Newtonsoft.Json.Linq;
using softWrench.sW4.Data;
using softWrench.sW4.Data.Persistence.Relational.EntityRepository;
using softWrench.sW4.Metadata;
using softWrench.sW4.Metadata.Applications;
using softWrench.sW4.Metadata.Entities.Sliced;
using softWrench.sW4.Metadata.Security;
using softwrench.sw4.offlineserver.events;
using softwrench.sw4.offlineserver.model.dto;
using softwrench.sw4.offlineserver.model.dto.association;
using softwrench.sw4.offlineserver.services.util;
using softwrench.sW4.Shared2.Metadata;
using softwrench.sW4.Shared2.Metadata.Applications;
using softwrench.sW4.Shared2.Metadata.Applications.Schema;
using softWrench.sW4.Configuration.Definitions;
using softWrench.sW4.Configuration.Services.Api;
using softWrench.sW4.Data.Offline;
using softWrench.sW4.Data.Persistence.Dataset.Commons;
using softWrench.sW4.Data.Persistence.Relational.Cache.Api;
using softWrench.sW4.Data.Persistence.Relational.QueryBuilder.Basic;
using softWrench.sW4.Data.Search;
using softWrench.sW4.Metadata.Stereotypes.Schema;
using softWrench.sW4.Security.Context;
using softWrench.sW4.Security.Services;
using softWrench.sW4.Util;
using SynchronizationResultDto = softwrench.sw4.offlineserver.model.dto.SynchronizationResultDto;

namespace softwrench.sw4.offlineserver.services {
    public class SynchronizationManager : ISingletonComponent {

        [Import]
        public DataSetProvider DataSetProvider { get; set; }

        private readonly OffLineCollectionResolver _resolver;
        private readonly EntityRepository _repository;
        private readonly IContextLookuper _lookuper;
        private readonly IEventDispatcher _iEventDispatcher;
        private readonly ISWDBHibernateDAO _swdbDAO;
        private readonly SyncChunkHandler _syncChunkHandler;
        private readonly IConfigurationFacade _configFacade;
        private readonly IDatamapRedisManager _redisManager;

        private static readonly ILog Log = LogManager.GetLogger(typeof(SynchronizationManager));

        public SynchronizationManager(OffLineCollectionResolver resolver, EntityRepository respository, IContextLookuper lookuper, IEventDispatcher iEventDispatcher, ISWDBHibernateDAO swdbDAO, SyncChunkHandler syncChunkHandler, IConfigurationFacade configFacade, IDatamapRedisManager redisManager) {
            _resolver = resolver;
            _repository = respository;
            _lookuper = lookuper;
            _iEventDispatcher = iEventDispatcher;
            _swdbDAO = swdbDAO;
            _syncChunkHandler = syncChunkHandler;
            _configFacade = configFacade;
            _redisManager = redisManager;
            Log.DebugFormat("init sync log");
        }


        public virtual async Task<SynchronizationResultDto> GetData(SynchronizationRequestDto request, InMemoryUser user) {
            _iEventDispatcher.Dispatch(new PreSyncEvent(request) { UpdateSwUserDb = true });

            var result = new SynchronizationResultDto { UserProperties = user.GenericSyncProperties };

            var rowstampMap = request.RowstampMap;
            var topLevelApps = GetTopLevelAppsToCollect(request, user);

            var tasks = new List<Task>();
            var compositionsToResolve = new ConcurrentBag<OffLineCollectionResolver.OfflineCollectionResolverParameters>();


            foreach (var topLevelApp in topLevelApps) {
                tasks.Add(Task.Run(async () => {
                    await ResolveApplication(request, user, topLevelApp, result, rowstampMap, compositionsToResolve);
                }));
            }

            await Task.WhenAll(tasks.ToArray());

            var mergedParameters = MergeCompositionsToResolve(compositionsToResolve);

            foreach (var compositionToResolveParameters in mergedParameters) {
                tasks.Add(Task.Run(async () => {
                    await DoResolveCompositions(result, compositionToResolveParameters);
                }));
            }

            await Task.WhenAll(tasks.ToArray());

            // add offline configs to the applications
            var configResultData = await GetOfflineConfigs();
            result.TopApplicationData.Add(configResultData);

            if (request.UserData != null) {
                result.FacilitiesUpdated = true;
            }

            result.ClientName = ApplicationConfiguration.ClientName;
            result.AttachmentCount = 0;

            if (request.DownloadAttachments) {
                result.AttachmentCount = CalculateAttachmentCount(result.CompositionData);
            }


            return result;
        }

        private int CalculateAttachmentCount(IList<SynchronizationApplicationResultData> resultCompositionData) {
            var attachments = resultCompositionData.FirstOrDefault(a => a.ApplicationName.EqualsIc("attachment_"));
            return attachments?.TotalCount ?? 0;
        }

        private Dictionary<string, OffLineCollectionResolver.OfflineCollectionResolverParameters>.ValueCollection MergeCompositionsToResolve(IEnumerable<OffLineCollectionResolver.OfflineCollectionResolverParameters> compositionsToResolve) {
            var mergedParameters = new Dictionary<string, OffLineCollectionResolver.OfflineCollectionResolverParameters>();

            foreach (var comp in compositionsToResolve) {
                var metadata = comp.ApplicationMetadata;
                var appName = metadata.Name;

                if (!appName.Contains("workorder")) {
                    //TODO: locate related entries from the metadata property application.original instead 
                    //and use a more professional logic (i.e ==> these different apps could have different composition sets)
                    mergedParameters.Add(appName, comp);
                    continue;
                }

                if (!mergedParameters.ContainsKey("workorder")) {
                    mergedParameters.Add("workorder", comp);
                } else {
                    mergedParameters["workorder"].Merge(comp);
                }
            }

            return mergedParameters.Values;
        }

        private async Task<SynchronizationApplicationResultData> GetOfflineConfigs() {
            var configs = await _swdbDAO.FindByQueryAsync<PropertyDefinition>("from PropertyDefinition where FullKey like '/Offline/%'");
            var datamaps = configs.Select(c => {
                var fields = new Dictionary<string, object>
                {
                    { "fullKey", c.FullKey },
                    { "stringValue" , c.StringValue},
                    { "defaultValue" , c.DefaultValue}
                };
                return JSONConvertedDatamap.FromFieldsAndMappingType("_configuration", fields);
            }).ToList();

            return new SynchronizationApplicationResultData("_configuration") {
                InsertOrUpdateDataMaps = datamaps
            };
        }

        public virtual async Task<AssociationSynchronizationResultDto> GetAssociationData(InMemoryUser currentUser, [NotNull]AssociationSynchronizationRequestDto request) {
            _iEventDispatcher.Dispatch(new PreSyncEvent(request));

            var applicationsToFetch = BuildAssociationsToFetch(currentUser, request);
            var dict = ClientStateJsonConverter.GetAssociationRowstampDict(request.RowstampMap);
            return await DoGetAssociationData(applicationsToFetch, dict, currentUser, request.CompleteCacheEntries, request.InitialLoad);
        }

        protected virtual IEnumerable<CompleteApplicationMetadataDefinition> BuildAssociationsToFetch(InMemoryUser currentUser,
            AssociationSynchronizationRequestDto request) {
            IEnumerable<CompleteApplicationMetadataDefinition> applicationsToFetch;
            var applicationNamesToFetch = request.ApplicationsToFetch;
            if (applicationNamesToFetch == null || !applicationNamesToFetch.Any()) {
                //let´s bring all the associations
                applicationsToFetch = OffLineMetadataProvider.FetchAssociationApps(currentUser, true);
                if (request.ApplicationsNotToFetch != null) {
                    applicationsToFetch =
                        applicationsToFetch.Where(a => !request.ApplicationsNotToFetch.Contains(a.ApplicationName));
                }
            } else {
                applicationsToFetch = applicationNamesToFetch.Select(d => MetadataProvider.Application(d));
            }
            return applicationsToFetch;
        }

        protected virtual async Task<AssociationSynchronizationResultDto> DoGetAssociationData(IEnumerable<CompleteApplicationMetadataDefinition> applicationsToFetch,
            [NotNull] IDictionary<string, ClientAssociationCacheEntry> rowstampMap, InMemoryUser user, IDictionary<string, CacheRoundtripStatus> completeCacheEntries, bool initialLoad) {
            var applicationsArray = applicationsToFetch.ToArray();
            var completeApplicationMetadataDefinitions = applicationsToFetch as CompleteApplicationMetadataDefinition[] ?? applicationsArray;

            var maxThreads = await _configFacade.LookupAsync<int>(OfflineConstants.MaxAssociationThreads);
            var chunkLimit = await _configFacade.LookupAsync<int>(OfflineConstants.MaxDownloadSize);

            var results = new AssociationSynchronizationResultDto(chunkLimit);

            IndexUtil.HandleIndexes(applicationsArray, results);

            if (initialLoad) {
                var cacheableApplications = completeApplicationMetadataDefinitions.Where(c => !"true".Equals(c.GetProperty(OfflineConstants.AvoidCaching))).ToList();
                var nonCacheableApplications = completeApplicationMetadataDefinitions.Where(c => "true".Equals(c.GetProperty(OfflineConstants.AvoidCaching))).ToList();
                foreach (var app in nonCacheableApplications) {
                    results.MarkAsIncomplete(app.ApplicationName);
                }

                await HandleCacheLookup(user, completeCacheEntries, cacheableApplications, maxThreads, results);
            }


            #region Database

            var watch = Stopwatch.StartNew();
            await HandleDatabaseAssociations(initialLoad, rowstampMap, user, results, completeApplicationMetadataDefinitions, maxThreads);

            results = await _syncChunkHandler.HandleMaxSize(results);

            if (!results.IncompleteAssociations.Any()) {
                results.HasMoreData = false;
            }

            Log.DebugFormat("SYNC:Finished handling all associations. Ellapsed {0}", LoggingUtil.MsDelta(watch));

            #endregion
            results.ClientName = ApplicationConfiguration.ClientName;

            return results;
        }

        private static Rowstamps BuildRowstamp(IDictionary<string, ClientAssociationCacheEntry> rowstampMap, AssociationSynchronizationResultDto results, CompleteApplicationMetadataDefinition association) {
            Rowstamps rowstamp = null;
            if (rowstampMap.ContainsKey(association.ApplicationName)) {
                var currentRowStamp = rowstampMap[association.ApplicationName].MaxRowstamp;
                var maxUid = rowstampMap[association.ApplicationName].MaxUid;
                rowstamp = new Rowstamps(currentRowStamp, null);
                if (!string.IsNullOrEmpty(maxUid)) {
                    rowstamp.LowerUid = maxUid;
                    rowstamp.Lowerlimit = null;
                }

            } else if (results.AssociationData.ContainsKey(association.ApplicationName) && results
                           .AssociationData[association.ApplicationName]
                           .CachedMaxRowstamp.HasValue) {
                var cacheRowstamp = results.AssociationData[association.ApplicationName].CachedMaxRowstamp;
                rowstamp = new Rowstamps(cacheRowstamp.ToString(), null);
            }


            return rowstamp;
        }

        internal async Task HandleDatabaseAssociations(bool initialLoad, IDictionary<string, ClientAssociationCacheEntry> rowstampMap, InMemoryUser user, AssociationSynchronizationResultDto results,
            IEnumerable<CompleteApplicationMetadataDefinition> completeApplicationMetadataDefinitions, int maxThreads) {

            var hasOverFlownOnCacheOperation = results.IsOverFlown();

            var applicationsToFetch = DatabaseApplicationsToCollect(initialLoad, results, completeApplicationMetadataDefinitions, hasOverFlownOnCacheOperation);

            var applicationMetadataDefinitions = applicationsToFetch as IList<CompleteApplicationMetadataDefinition> ?? applicationsToFetch.ToList();

            if (applicationMetadataDefinitions.Any()) {
                //after the cache has finished, regardless whether it has overflow, let´s force bringing automatically any values bigger than the cached rowstamp
                //for instance (cache brought all numericdomains until the rowstamp x --> now it´s time to query the eventual existing bigger rowstampped items)
                //these caches are way faster (due to rowstamp constraints), and shouldn´t return a meaningful number of results to impact the chunk size


                var tasks = new Task[Math.Min(applicationMetadataDefinitions.Count, maxThreads)];

                var j = 0;

                while (j < applicationMetadataDefinitions.Count) {

                    //if it has overflown, but not due to a cache hit
                    if (!hasOverFlownOnCacheOperation && results.IsOverFlown()) {
                        var association = applicationMetadataDefinitions[j++];
                        //marking this association to be downloaded on a next chunk 
                        results.MarkAsIncomplete(association.ApplicationName);
                        continue;
                    }


                    Log.DebugFormat("initing another round of database lookup, since we still have some space");

                    var i = 0;
                    var cacheTasks = new Task<RedisLookupResult<JSONConvertedDatamap>>[Math.Min(applicationMetadataDefinitions.Count - j, maxThreads)];

                    while (i < cacheTasks.Length && j < applicationMetadataDefinitions.Count) {
                        var association = applicationMetadataDefinitions[j++];
                        var schemaKey = new ApplicationMetadataSchemaKey(ApplicationMetadataConstants.List, SchemaMode.None, ClientPlatform.Mobile);
                        var userAppMetadata = association.ApplyPolicies(schemaKey, user, ClientPlatform.Mobile);
                        var rowstamp = BuildRowstamp(rowstampMap, results, association);
                        tasks[i++] = Task.Run(async () => {
                            await InnerGetAssocData(association, userAppMetadata, rowstamp, results);
                        });
                    }

                    await Task.WhenAll(tasks);
                }
            } else {
                Log.DebugFormat("sync: Ingnoring association database queries, since the cache already fullfilled the chunk");
            }
        }

        //either all the incomplete applications, or, if the chunk limit was reached, only those where the cache lookup has exhausted
        internal virtual ISet<CompleteApplicationMetadataDefinition> DatabaseApplicationsToCollect(bool initialLoad, AssociationSynchronizationResultDto resultDTO,
            IEnumerable<CompleteApplicationMetadataDefinition> completeApplicationMetadataDefinitions, bool hasOverFlownOnCacheOperation) {

            var results = new HashSet<CompleteApplicationMetadataDefinition>();

            var definitions = completeApplicationMetadataDefinitions as IList<CompleteApplicationMetadataDefinition> ?? completeApplicationMetadataDefinitions.ToList();
            var nonCacheable = definitions.Where(c => "true".Equals(c.GetProperty(OfflineConstants.AvoidCaching))).OrderBy(s => s.ApplicationName).ToList();

            if (!initialLoad) {
                if (!_redisManager.IsAvailable()) {
                    //cache is no longer available for some reason, we should ensure to bring it all via the database
                    return new LinkedHashSet<CompleteApplicationMetadataDefinition>(definitions);
                }

                return new LinkedHashSet<CompleteApplicationMetadataDefinition>(nonCacheable);
            }

            if (hasOverFlownOnCacheOperation && resultDTO.HasMoreCacheData) {
                Log.DebugFormat("database operations will be ignored since there are still more cached entries to be collected");
                return results;
            }

            if (!hasOverFlownOnCacheOperation) {
                results.AddAll(nonCacheable);
                results.AddAll(definitions.Where(c => resultDTO.CacheMiss(c.ApplicationName)));
            }

            //small datasets shall also be added, regardless of the main downloadchunk limit being overflown or not
            results.AddAll(nonCacheable.Where(c => "true".Equals(c.GetProperty(OfflineConstants.SmallDataSet))));


            if (hasOverFlownOnCacheOperation && resultDTO.CompleteCacheEntries.Any()) {
                //if the download chunk overflown, let´s return only the diferential database entries on top of the cache rowstamps
                //assuming there sould be few entries to collect, due to the rowstamp filtering
                results.AddAll(definitions.Where(
                    c => resultDTO.HasFinishedCollectingCache(c.ApplicationName) &&
                         ShouldCheckDatabaseAfterCache(c)));
            } else if (!resultDTO.HasMoreCacheData) {
                if (!_redisManager.IsAvailable()) {
                    return new LinkedHashSet<CompleteApplicationMetadataDefinition>(definitions);
                }

                //there´s still space on the downloadchunk, and there are no more cache entries to check--> let´s fetch all the database entries 
                //for the apps which were NOT taken already from the cache, or for the ones that had a cache miss
                // The not idea is to avoid doing/bringing useless database queries for things that were already fetched from the cache on the initial load
                //i.e if I´m already bringing all the locations from the cache why bothering querying for it again at this point, even when we know there´s space left
                results.AddAll(definitions.Where(c => (!resultDTO.HasFinishedCollectingCache(c.ApplicationName) || resultDTO.CacheMiss(c.ApplicationName))));
            }

            return results;
        }

        protected virtual bool ShouldCheckDatabaseAfterCache(CompleteApplicationMetadataDefinition completeApplicationMetadataDefinition) {
            return "true".Equals(completeApplicationMetadataDefinition.GetProperty(OfflineConstants.CheckDatabaseAfterCache));
        }


        protected virtual async Task HandleCacheLookup(InMemoryUser user, IDictionary<string, CacheRoundtripStatus> completeCacheEntries, IList<CompleteApplicationMetadataDefinition> completeApplicationMetadataDefinitions, int maxThreads, AssociationSynchronizationResultDto results) {

            if (!_redisManager.IsAvailable()) {
                return;
            }

            var watch = Stopwatch.StartNew();

            var j = 0;


            Log.DebugFormat("init association cache lookup process");

            //until we reach the chunk limit, or we exhaust all the applications, let´s add more data into the results
            //limiting the threads to the specified configuration, to avoid a lack of resources on the IIS server
            while (j < completeApplicationMetadataDefinitions.Count) {

                if (results.IsOverFlown()) {
                    var association = completeApplicationMetadataDefinitions[j++];
                    results.MarkAsIncomplete(association.ApplicationName);
                    continue;
                }

                Log.DebugFormat("initing another round of cache lookup, since we still have some space");

                var i = 0;
                var cacheTasks = new Task[Math.Min(completeApplicationMetadataDefinitions.Count - j, maxThreads)];

                var redisResults = new List<RedisLookupResult<JSONConvertedDatamap>>();
                while (i < cacheTasks.Length && j < completeApplicationMetadataDefinitions.Count) {
                    var association = completeApplicationMetadataDefinitions[j++];
                    var schemaKey = new ApplicationMetadataSchemaKey(ApplicationMetadataConstants.List, SchemaMode.None, ClientPlatform.Mobile);
                    var userAppMetadata = association.ApplyPolicies(schemaKey, user, ClientPlatform.Mobile);
                    var lookupDTO = await BuildRedisDTO(userAppMetadata, completeCacheEntries);

                    cacheTasks[i++] = Task.Run(async () => {
                        redisResults.Add(await _redisManager.Lookup<JSONConvertedDatamap>(lookupDTO));
                    });
                }

                await Task.WhenAll(cacheTasks);
                foreach (var redisResult in redisResults) {
                    //each application might span multiple chunks of data, although it would be abnormal (ex: change in the chunk size)
                    results.AddJsonFromRedisResult(redisResult, "true".Equals(redisResult.Schema.GetProperty(OfflineConstants.CheckDatabaseAfterCache)));
                }
            }

            Log.InfoFormat("Cache lookup profess completed in {0}", LoggingUtil.MsDelta(watch));

        }


        protected virtual async Task InnerGetAssocData(CompleteApplicationMetadataDefinition association, ApplicationMetadata userAppMetadata, Rowstamps rowstamp, AssociationSynchronizationResultDto results) {
            var entityMetadata = MetadataProvider.SlicedEntityMetadata(userAppMetadata);
            var context = _lookuper.LookupContext();
            context.OfflineMode = true;
            _lookuper.AddContext(context);
            var isLimited = association.GetProperty("mobile.fetchlimit") != null;
            results.LimitedAssociations.Add(userAppMetadata.Name, isLimited);

            var datamaps = await FetchData(true, entityMetadata, userAppMetadata, rowstamp, null, isLimited);

            results.AddIndividualJsonDatamaps(association.ApplicationName, association.IdFieldName, datamaps);
        }

        protected virtual async Task ResolveApplication(SynchronizationRequestDto request, InMemoryUser user, CompleteApplicationMetadataDefinition topLevelApp, SynchronizationResultDto result, JObject rowstampMap,
            ConcurrentBag<OffLineCollectionResolver.OfflineCollectionResolverParameters> compositionsToResolve) {
            var watch = Stopwatch.StartNew();
            //this will return sync schema
            var userAppMetadata = topLevelApp.ApplyPolicies(ApplicationMetadataSchemaKey.GetSyncInstance(), user, ClientPlatform.Mobile);

            var entityMetadata = MetadataProvider.SlicedEntityMetadata(userAppMetadata);
            var rowstampDTO = ClientStateJsonConverter.ConvertJSONToDict(rowstampMap);

            var appRowstampDTO = LocateAppRowstamp(topLevelApp, rowstampDTO);

            Rowstamps rowstamps = null;
            if (appRowstampDTO.MaxRowstamp != null) {
                rowstamps = new Rowstamps(appRowstampDTO.MaxRowstamp, null);
            }
            var isQuickSync = request.ItemsToDownload != null;

            var isLimited = topLevelApp.GetProperty("mobile.fetchlimit") != null;

            var topLevelAppData = await FetchData(false, entityMetadata, userAppMetadata, rowstamps, request.ItemsToDownload, isLimited);


            var appResultData = FilterData(topLevelApp.ApplicationName, topLevelAppData, appRowstampDTO, topLevelApp, isQuickSync);

            result.AddTopApplicationData(appResultData);
            Log.DebugFormat("SYNC:Finished handling top level app. Ellapsed {0}", LoggingUtil.MsDelta(watch));

            if (!appResultData.IsEmptyExceptDeletion) {
                MarkCompositionsToSolve(userAppMetadata, appResultData, result, rowstampMap, compositionsToResolve);
            }

        }

        private static ClientStateJsonConverter.AppRowstampDTO LocateAppRowstamp(CompleteApplicationMetadataDefinition topLevelApp, List<ClientStateJsonConverter.AppRowstampDTO> rowstampDTO) {
            ClientStateJsonConverter.AppRowstampDTO appRowstampDTO = null;

            if (rowstampDTO.Count == 1) {
                appRowstampDTO = rowstampDTO[0];
            } else {
                appRowstampDTO = rowstampDTO.FirstOrDefault(f => f.ApplicationName == topLevelApp.ApplicationName);
            }
            if (appRowstampDTO == null) {
                appRowstampDTO = new ClientStateJsonConverter.AppRowstampDTO();
            }
            return appRowstampDTO;
        }

        ///  <summary>
        ///  Brings all composition data related to the main application passed as parameter (i.e only the ones whose parent entites are fetched).
        ///  
        ///  If there´s no new parent entity, we can safely bring only compositions greater than the max rowstamp cached in the client side; 
        ///  
        ///  If new entries appeared, however, for the sake of simplicity we won´t use this strategy since the whereclause might have changed in the process, causing loss of data
        ///  
        /// 
        ///  </summary>
        ///  <param name="topLevelApp"></param>
        ///  <param name="appResultData"></param>
        ///  <param name="result"></param>
        ///  <param name="rowstampMap"></param>
        /// <param name="toResolve"></param>
        protected virtual void MarkCompositionsToSolve(ApplicationMetadata topLevelApp, SynchronizationApplicationResultData appResultData, SynchronizationResultDto result, JObject rowstampMap, ConcurrentBag<OffLineCollectionResolver.OfflineCollectionResolverParameters> toResolve) {


            var compositionMap = ClientStateJsonConverter.GetCompositionRowstampsDict(rowstampMap);

            if (!appResultData.AllData.Any()) {
                return;
            }

            var parameters = new OffLineCollectionResolver.OfflineCollectionResolverParameters(topLevelApp, appResultData.AllData, compositionMap, appResultData.NewdataMaps.Select(s => s.OriginalDatamap), appResultData.AlreadyExistingDatamaps);
            toResolve.Add(parameters);


            //            await DoResolveCompositions(result, parameters, watch);
        }

        private async Task DoResolveCompositions(SynchronizationResultDto result, OffLineCollectionResolver.OfflineCollectionResolverParameters parameters) {

            var watch = Stopwatch.StartNew();

            var compositionData = await _resolver.ResolveCollections(parameters);


            foreach (var compositionDict in compositionData) {
                var dict = compositionDict;
                //lets assume no compositions can be updated, for the sake of simplicity
                var newDataMaps = new List<JSONConvertedDatamap>();
                foreach (var list in compositionDict.Value.ResultList) {
                    newDataMaps.Add(JSONConvertedDatamap.FromFields(dict.Key, list, dict.Value.IdFieldName));
                }

                var resultData = new SynchronizationApplicationResultData(dict.Key, newDataMaps, null);
                var compositionSchema = parameters.CompositionSchemas[compositionDict.Key];
                var schemas = compositionSchema.Schemas;
                var anySchema = schemas.List ?? schemas.Detail ?? schemas.Print ?? schemas.Sync;
                var compositionApp = MetadataProvider.Application(anySchema.ApplicationName);
                IndexUtil.ParseIndexes(resultData.TextIndexes, resultData.NumericIndexes, resultData.DateIndexes,
                    compositionApp);

                result.AddCompositionData(resultData);
            }
            Log.DebugFormat("SYNC:Finished handling compositions. Ellapsed {0}", LoggingUtil.MsDelta(watch));
        }

        protected virtual IEnumerable<CompleteApplicationMetadataDefinition> GetTopLevelAppsToCollect(SynchronizationRequestDto request, InMemoryUser user) {
            var applicationName = request.ApplicationName;

            var topLevelApps = MetadataProvider.FetchTopLevelApps(ClientPlatform.Mobile, user).Where(a => !a.IsPropertyTrue(OfflineConstants.IgnoreAsTopApp));

            if (applicationName == null) {
                //no application in special was requested, lets return them all.
                return topLevelApps;
            }
            if (request.ReturnNewApps) {
                if (request.ClientCurrentTopLevelApps != null) {
                    var result = new HashSet<CompleteApplicationMetadataDefinition>();
                    var otherApps = topLevelApps.Where(a => !request.ClientCurrentTopLevelApps.Contains(a.ApplicationName));
                    result.AddAll(otherApps);
                    result.Add(MetadataProvider.Application(applicationName));
                    return result;
                }
                return topLevelApps;
            }
            var mainApplication = MetadataProvider.Application(applicationName);
            var resultList = new List<CompleteApplicationMetadataDefinition> { mainApplication };
            resultList.AddRange(topLevelApps.Where(a => applicationName.EqualsIc(a.GetProperty(ApplicationSchemaPropertiesCatalog.OriginalApplication))));

            return resultList;
        }





        protected virtual SynchronizationApplicationResultData FilterData(string applicationName, ICollection<JSONConvertedDatamap> topLevelAppData, ClientStateJsonConverter.AppRowstampDTO rowstampDTO,
            CompleteApplicationMetadataDefinition topLevelApp, bool isQuickSync) {
            var watch = Stopwatch.StartNew();

            var result = new SynchronizationApplicationResultData(applicationName) {
                AllData = topLevelAppData.Select(t => t.OriginalDatamap).ToList(),
            };

            IndexUtil.ParseIndexes(result.TextIndexes, result.NumericIndexes, result.DateIndexes, topLevelApp);

            if (rowstampDTO.MaxRowstamp != null || isQuickSync) {
                //SWOFF-140 
                result.InsertOrUpdateDataMaps = topLevelAppData;
                return result;
            }

            //this is the full strategy implementation where the client passes the whole state on each synchronization
            var idRowstampDict = rowstampDTO.ClientState;

            var avoidIncremental = _configFacade.Lookup<bool>(OfflineConstants.AvoidIncrementalSync);

            if (idRowstampDict == null || idRowstampDict.Count == 0) {
                //this should happen for first synchronization, of for "full-forced-synchronization"
                result.NewdataMaps = topLevelAppData;
                return result;
            }

            //            if (avoidIncremental) {
            //                result.InsertOrUpdateDataMaps = topLevelAppData;
            //                return result;
            //            }

            //            var idRowstampDict = ClientStateJsonConverter.ConvertJSONToDict(rowstampMap);
            foreach (var dataMap in topLevelAppData) {
                var id = dataMap.Id;
                if (!idRowstampDict.ContainsKey(id)) {
                    Log.DebugFormat("sync: adding inserteable item {0} for application {1}", dataMap.Id, applicationName);
                    result.NewdataMaps.Add(dataMap);
                } else {
                    //this is for composition handling, where we´d search all compositions considering the whole list of main entities
                    result.AlreadyExistingDatamaps.Add(dataMap.OriginalDatamap);
                    var rowstamp = idRowstampDict[id];
                    if (!rowstamp.Equals(dataMap.Approwstamp.ToString())) {
                        Log.DebugFormat("sync: adding updateable item {0} for application {1}", dataMap.Id, applicationName);
                        result.InsertOrUpdateDataMaps.Add(dataMap);
                    }
                    //removing so that the remaining items are the deleted ids --> avoid an extra loop
                    idRowstampDict.Remove(id);
                }
            }


            Log.DebugFormat("sync: {0} items to delete for application {1}", result.DeletedRecordIds.Count, applicationName);
            result.DeletedRecordIds = idRowstampDict.Keys;

            Log.DebugFormat("sync: filter data for {0} ellapsed {1}", applicationName, LoggingUtil.MsDelta(watch));
            return result;
        }






        protected virtual async Task<List<JSONConvertedDatamap>> FetchData(bool isAssociationData, SlicedEntityMetadata entityMetadata, ApplicationMetadata appMetadata,
            Rowstamps rowstamps = null, List<string> itemsToDownload = null, bool isLimited = false) {

            var qualifier = ApplicationConfiguration.IsOracle(entityMetadata.DbType) ? entityMetadata.Name + "." : "";

            var searchDto = new SearchRequestDto {
                SearchSort = isLimited ? "{0}rowstamp desc".Fmt(qualifier) : "{0}rowstamp asc".Fmt(qualifier),
                Key = new ApplicationMetadataSchemaKey {
                    ApplicationName = appMetadata.Name
                }
            };

            if (isAssociationData && (rowstamps == null || rowstamps.LowerUid != null)) {
                //initial asociation download, using uid ascending, so that we can cache the results if needed be
                searchDto.SearchSort = "{0} {1}".Fmt(appMetadata.Schema.IdFieldName, isLimited ? "desc" : "asc");
                if (rowstamps != null && rowstamps.LowerUid != null) {
                    //TODO: move to inner framework at SearchUtils,perhaps, based on rowstamps
                    searchDto.AppendWhereClauseFormat(" {0} > {1} ", appMetadata.Schema.IdFieldName, rowstamps.LowerUid);
                }

            }



            if (rowstamps == null) {
                rowstamps = new Rowstamps();
            }


            var avoidIncremental = _configFacade.Lookup<bool>(OfflineConstants.AvoidIncrementalSync);

            var cacheableItem = !"true".Equals(appMetadata.Schema.GetProperty(OfflineConstants.AvoidCaching));
            var avoidIncrementalApp = "true".Equals(appMetadata.Schema.GetProperty(OfflineConstants.AvoidIncrementalApp));

            if (avoidIncremental && !cacheableItem && avoidIncrementalApp) {
                rowstamps = new Rowstamps();
            }

            if (itemsToDownload != null) {
                var notEmpty = itemsToDownload.Where(item => !string.IsNullOrEmpty(item)).ToList();
                if (!notEmpty.Any()) {
                    // QuickSync of created application - for now it's impossible a created application stay after sync
                    // TODO: fix the fetch of newly created application
                    searchDto.AppendWhereClause("1!=1");
                } else {
                    //ensure only the specified items are downloaded
                    searchDto.AppendWhereClauseFormat("{0} in ({1})", appMetadata.IdFieldName, BaseQueryUtil.GenerateInString(notEmpty));
                }
            }

            searchDto.QueryAlias = "sync:" + appMetadata.Name;

            var enumerable = await _repository.GetSynchronizationData(entityMetadata, rowstamps, searchDto);
            if (!enumerable.Any()) {
                return new List<JSONConvertedDatamap>();
            }
            var dataMaps = new List<JSONConvertedDatamap>();
            foreach (var row in enumerable) {
                var dataMap = DataMap.Populate(appMetadata, entityMetadata, row);
                dataMaps.Add(new JSONConvertedDatamap(dataMap, false, appMetadata));
            }

            if (isAssociationData && cacheableItem) {

                //updating cache entries

                var lookupDTO = await BuildRedisDTO(appMetadata, null);
                var inputDTO = new RedisInputDTO<JSONConvertedDatamap>(dataMaps);
#pragma warning disable 4014
                //updating cache on background thread
                Task.Run(() => _redisManager.InsertIntoCache(lookupDTO, inputDTO));
#pragma warning restore 4014
            }

            return dataMaps;
        }

        protected virtual async Task<RedisLookupDTO> BuildRedisDTO(ApplicationMetadata appMetadata, IDictionary<string, CacheRoundtripStatus> completeCacheEntries) {

            var maxSize = await _configFacade.LookupAsync<int>(OfflineConstants.MaxDownloadSize);
            var user = SecurityFacade.CurrentUser();

            var lookupDTO = new RedisLookupDTO {
                Schema = appMetadata.Schema,
                IsOffline = true,
                GlobalLimit = maxSize,
                CacheRoundtripStatuses = completeCacheEntries
            };
            lookupDTO.ExtraKeys.Add("siteid", user.SiteId);
            lookupDTO.ExtraKeys.Add("orgid", user.OrgId);

            return lookupDTO;
        }



    }
}
