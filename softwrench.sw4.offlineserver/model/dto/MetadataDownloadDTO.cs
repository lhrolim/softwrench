﻿namespace softwrench.sw4.offlineserver.model.dto {
    public class MetadataDownloadDto {

        public string ClientOperationId { get; set; }

        public DeviceData DeviceData { get; set; }

        public string Version { get; set; }
    }
}
