﻿namespace softwrench.sw4.firstsolar.classes.com.cts.firstsolar.dataset {
    public class FSWPackageConstants {
        public const string WorklogsRelationship = "wkpgworklogs_";
        public const string AttachsRelationship = "wkpgattachments_";
        public const string CallOutAttachsRelationship = "wkpgcoattachments_";
        public const string MaintenanceEngAttachsRelationship = "wkpgmeattachments_";
        public const string DailyOutageMeetingAttachsRelationship = "wkpgdomattachments_";
        public const string AllAttachmentsRelationship = "wkpgallattachments_";
        public const string RelatedWorkOrdersRelationship = "wkpgrelatedwos_";
    }
}
