﻿namespace softwrench.sw4.firstsolar.classes.com.cts.firstsolar.configuration {
    public class FirstSolarConstants {

        public const string FacilityAdmin = "onlinefacilityadmin";

        public const string SecondaryOrg = "sync.secondaryorg";
        public const string SecondarySite = "sync.secondarysite";
        public const string FacilitiesProp = "sync.facilities";
        
        public const string AvailableFacilitiesProp = "sync.availablefacilities";
        public const string TierOneCcEmails = "Troy.Lauterbach@firstsolar.com, TMoeller@firstsolar.com, Frank.Kelly@firstsolar.com, Kirby.Hunt@firstsolar.com, Tom.Studer@firstsolar.com, William.Byrd@firstsolar.com";
    }
}
