﻿
(function (angular) {
    'use strict';



    function batchWorkorderService($rootScope, $q, $log, crudContextHolderService, modalService, associationService, validationService, restService, alertService, contextService, redirectService,submitServiceCommons) {


        // save method of inline wo edit modal
        function woInlineEditSave(saveDataMap, schema) {
            const validationErrors = validationService.validate(schema, schema.displayables, saveDataMap, {});
            if (validationErrors.length > 0) {
                return;
            }
            const params = {
                batchType: saveDataMap.userIdFieldName === "location" ? "location" : "asset",
                specificValue: saveDataMap[saveDataMap.userIdFieldName],
                classificationId: saveDataMap["classificationid"],
                worktype: saveDataMap["worktype"]
            };
            var resultObject;

            restService.postPromise("FirstSolarWorkorderBatch", "ValidateExistingWorkorders", params).then(function (httpResponse) {
                resultObject = httpResponse.data.resultObject;
                submitServiceCommons.insertAssocationLabelsIfNeeded(schema, saveDataMap);
                const gridDatamap = crudContextHolderService.rootDataMap();
                gridDatamap.forEach(function (row) {
                    if (row[schema.idFieldName] === saveDataMap[schema.idFieldName]) {
                        row.summary = saveDataMap.summary;
                        row.details = saveDataMap.details;
                        row.siteid = saveDataMap.siteid;
                        row.classification = saveDataMap["#classificationid_label"] || "";
                        row.classificationid = saveDataMap.classificationid;
                        row.worktype = saveDataMap.worktype;
                        row["#warning"] = resultObject["#warning"];
                        row["#wonums"] = resultObject["#wonums"];
                    }
                });
                modalService.hide();
            });
        }

        // save method of pre wo batch creation
        function woBatchSharedSave(schema, modalData, modalSchema) {


            submitServiceCommons.insertAssocationLabelsIfNeeded(modalSchema, modalData);
            var selectionBuffer = crudContextHolderService.getSelectionModel().selectionBuffer;
            const batchData = {
                summary: modalData["summary"],
                details: modalData["details"],
                siteid: modalData["siteid"],
                classification: {
                    value: modalData["classificationid"],
                    label: modalData["#classificationid_label"]
                },
                worktype: modalData["worktype"]
            };
            batchData["items"] = Object.keys(selectionBuffer).map(function (key) {
                const value = selectionBuffer[key];
                const associationOption = {
                    value: value[schema.userIdFieldName],
                    label: value["description"]
                };
                associationOption.extraFields = {
                    "siteid": value["siteid"],
                    "orgid": value["orgid"]
                }

                if (schema.applicationName === "asset") {
                    //passing location of the asset as an extra projection field
                    associationOption.extraFields["location"] = value["location"];
                }
                return associationOption;
            });
            const params = {
                batchType: schema.applicationName === "asset" ? "asset" : "location"
            };
            return restService.postPromise("FirstSolarWorkorderBatch", "InitBatch", params, batchData);
        }

        function proceedToBatchSelection(httpResponse, confirmMessage) {
            const applicationResponse = httpResponse.data;
            //            if (applicationResponse.extraParameters && true === applicationResponse.extraParameters["allworkorders"]) {
            //                return alertService.confirm2(confirmMessage)
            //                    .then(function () {
            //                        //storing untouched first line to serve as shared data later
            //                        contextService.set("batchshareddata", applicationResponse.resultObject[0].fields, true);
            //                        return redirectService.redirectFromServerResponse(applicationResponse);
            //
            //                    }).catch(function () {
            //                        //catching exception in order to close the modal on the outer promise handler
            //                        return;
            //                    });
            //            }
            contextService.set("batchshareddata", applicationResponse.resultObject[0], true);
            return redirectService.redirectFromServerResponse(applicationResponse);
        }

        function spreadSheetLineClick(rowDm, column, schema) {
            const wonums = rowDm["#wonums"];
            if (column.attribute === "#warning" && wonums) {
                loadRelatedWorkorders(rowDm, wonums);
                return false;
            }
            const nextSchemaId = schema.properties["list.click.schema"];
            redirectService.openAsModal("workorder", nextSchemaId, null, rowDm);
            return false;
        }

        function loadRelatedWorkorders(rowDm, wonums) {
            const commaSeparattedQuotedIds = wonums.split(',').map(function (item) {
                return "'" + item + "'";
            }).join(",");
            var fixedWhereClause = "wonum in ({0})".format(commaSeparattedQuotedIds);
            const params = {
                searchDTO: {
                    filterFixedWhereClause: fixedWhereClause,
                    pageSize:10
                },
                title: "Work Orders of " + rowDm["specificLabel"]
            };
            redirectService.openAsModal("workorder", "readonlyfixedlist", params).then(function () {
                crudContextHolderService.setFixedWhereClause("#modal", fixedWhereClause);
            });
        }

        function submitBatch(batchType) {
            const log = $log.get("batchWorkorderService#submitBatch", ["workorder"]);
            log.debug("init batch submission process for {0}".format(batchType));

            var keyName = batchType === "asset" ? "assetnum" : "location";

            var itemsToSubmit = crudContextHolderService.getSelectionModel().selectionBuffer;
            const itemsToSubmitKeys = Object.keys(itemsToSubmit);
            var sharedData = contextService.get("batchshareddata", false, true);
            var specificData = {};
            const submissionData = {
                sharedData: sharedData,
                specificData: specificData
            };
            if (itemsToSubmitKeys.length === 0) {
                alertService.alert("Please, select at least one entry to confirm the batch");
                return $q.reject();
            }

            itemsToSubmitKeys.forEach(function (bufferKey) {
                const datamap = itemsToSubmit[bufferKey];
                var fields = datamap;
                const customizedValues = Object.keys(fields).filter(function (prop) {
                    return prop !== keyName && fields[prop] !== sharedData[prop];
                });
                var key = fields[keyName];

                if (customizedValues.length !== 0) {
                    specificData[key] = {};
                    customizedValues.forEach(function (prop) {
                        specificData[key][prop] = fields[prop];
                    });
                } else {
                    specificData[key] = null;
                }

            });
            const params = { batchType: batchType };
            return restService.postPromise("FirstSolarWorkorderBatch", "SubmitBatch", params, JSON.stringify(submissionData))
                .then(function (httpResponse) {
                    const appResponse = httpResponse.data;
                    return redirectService.redirectFromServerResponse(appResponse, "workorder");
                });

        }

        const service = {
            woInlineEditSave: woInlineEditSave,
            woBatchSharedSave: woBatchSharedSave,
            proceedToBatchSelection: proceedToBatchSelection,
            spreadSheetLineClick: spreadSheetLineClick,
            submitBatch: submitBatch
        };
        return service;
    }

    angular
      .module('firstsolar')
      .clientfactory('batchWorkorderService', ['$rootScope', "$q", "$log", 'crudContextHolderService', 'modalService', 'associationService', 'validationService', 'restService', 'alertService', 'contextService', 'redirectService', 'submitServiceCommons', batchWorkorderService]);
})(angular);