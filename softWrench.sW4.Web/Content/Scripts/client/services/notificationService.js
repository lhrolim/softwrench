﻿
(function (angular) {
    'use strict';

    angular.module('sw_prelogin').service('notificationViewModel', ['contextService', '$timeout', '$log', '$injector', '$rootScope', notificationViewModel]);
    function notificationViewModel(contextService, $timeout, $log, $injector, $rootScope) {
        var log = $log.getInstance('sw4.notificationViewModel');

        //#region Utils
        var vm = {
            messages: []
        };

        //#endregion
        //#region Private Methods

        function getMessages() {
            return vm.messages;
        }

        $rootScope.$on(JavascriptEventConstants.AppBeforeRedirection, function () {
            vm.messages.forEach(m=> m.display = false);
        });


        return {
            /// <summary>
            /// Receive message data and crete user notification
            /// </summary>
            /// <param name="type" type="string">Optional, [dev, error, info (default), null, success]</param>
            /// <param name="title" type="string">Optional, if no value the default title will be used based on the data.type. For consistency the default title should be used</param>
            /// <param name="body" type="string">Required, message text to display</param>
            /// <param name="exceptionType" type="string">Optional, display in more info modal</param>
            /// <param name="exceptionOutline" type="string">Optional, display in more info modal</param>
            /// <param name="exceptionStack" type="string">Optional, display in more info modal</param>
            /// <returns></returns>
            createNotification: function (type, title, body, exceptionType, exceptionOutline, exceptionStack) {

                //build the message object
                var message = {};
                message.type = type;
                message.title = title;
                message.body = body;
                message.display = true;

                //if any exception info is present, create the exception object
                if (exceptionType || exceptionOutline || exceptionStack) {
                    const exception = {};
                    exception.type = exceptionType;
                    exception.outline = exceptionOutline;
                    exception.stack = exceptionStack;
                    message.exception = exception;
                }

                //broad the notification event
                if (message.body) {
                    log.debug('createNotification', message);

                    vm.messages.push(message);
                    $timeout(function () {
                        //timeout to display messages
                    });

                    //add automatic timeout for success messages
                    if (message.type === 'success' && !message.exception) {
                        $timeout(function () {
                            //$scope.removeMessage(message);
                            message.display = false;
                        }, contextService.retrieveFromContext('successMessageTimeOut'));
                    }
                }
            },

            /// <summary>
            /// Return the data for the more info modal
            /// </summary>
            /// <param name="message" type="object">Notification message</param>
            /// <returns></returns>
            getMoreInfo: function (message) {
                //setup more info temporary store
                const moreInfo = message.exception;
                moreInfo.title = message.body;
                moreInfo.text = ('Error description:\n\n' +
                    'Type: \n{0}\n\n' +
                    'Message: \n{1}\n\n' +
                    'Outline:\n{2}\n\n' +
                    'StackTrace:\n{3}\n\n')
                .format(moreInfo.type, moreInfo.title, moreInfo.outline, moreInfo.stack);

                return moreInfo;
            },

            /// <summary>
            /// Display the JS error notification
            /// </summary>
            /// <param name="jsEvent" type="object">JS error event</param>
            /// <param name="angularException" type="string">Angular error exception</param>
            /// <returns></returns>
            processJsError: function (jsEvent, angularException) {
                //TODO: Replace $injector with configurationService, after circular dependency is fixed
                const configService = $injector.get('configurationService');
                var showAlert = true;

                //if there already is a JS error, don't show another notification
                this.messages.forEach(function (notification) {
                    if (notification.type === 'dev' && notification.display) {
                        showAlert = false;
                    }
                });
                if (!showAlert) {
                    return;
                }

                //check config values
                const jsErrorAlertShowDev = configService.getConfigurationValue('/Global/JsError/ShowDev') === 'true';
                const jsErrorAlertShowProd = configService.getConfigurationValue('/Global/JsError/ShowProd') === 'true';

                const isDevorQaEnv = $rootScope.environment && ($rootScope.environment.indexOf('dev') >= 0 || $rootScope.environment.indexOf('qa') >= 0);

                if (!isDevorQaEnv && !jsErrorAlertShowProd) {
                    return;
                }

                if (isDevorQaEnv && !jsErrorAlertShowDev) {
                    return;
                }

                var body;
                var exceptionType;
                var exceptionOutline;
                var exceptionStack;

                //get the javascript event values
                if (jsEvent) {
                    const e = jsEvent.originalEvent;
                    body = e.message;
                    exceptionType = e.type;
                    const error = e.error;
                    if (!error) {
                        return;
                    }
                    exceptionOutline = error.message;
                    exceptionStack = error.stack;

                    const parts = e.message.split(': ');
                    if (parts[0]) {
                        exceptionType = parts[0];
                    }
                }

                //get the angular values
                if (angularException ) {
                    body = angularException;
                }

                this.createNotification('dev', null, body, exceptionType, exceptionOutline, exceptionStack);
            },

            /// <summary>
            /// Hide the user selected message
            /// </summary>
            /// <param name="message" type="object">Notification message</param>
            /// <returns></returns>
            removeNotification: function (message) {
                log.debug('removeMessage', message);
                message.display = false;
            },


            messages: getMessages()
        }



        //#endregion
    }
})(angular);
