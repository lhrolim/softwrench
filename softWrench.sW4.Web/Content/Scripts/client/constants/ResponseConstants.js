﻿const BlankApplicationResponse = "BlankApplicationResponse";
const GenericApplicationResponse = "GenericApplicationResponse";
const ApplicationListResult = "ApplicationListResult";
const ApplicationDetailResult = "ApplicationDetailResult";
const NotFoundResponse = "NotFoundResponse";
const ActionRedirectResponse = "ActionRedirectResponse";


class ResponseConstants {
    static get BlankApplicationResponse() {
        return BlankApplicationResponse;
    }

    static get GenericApplicationResponse() {
        return GenericApplicationResponse;
    }

    static get ApplicationListResult() {
        return ApplicationListResult;
    }

    static get ApplicationDetailResult() {
        return ApplicationDetailResult;
    }

    static get NotFoundResponse() {
        return ApplicationDetailResult;
    }

    static get ActionRedirectResponse() {
        return ActionRedirectResponse;
    }
}