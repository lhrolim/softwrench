﻿const OptionField = "OptionField";
const AssociationField = "ApplicationAssociationDefinition";


class MetadataConstants {

    static get OptionField() {
        return OptionField;
    }

    static get AssociationField() {
        return AssociationField;
    }
}