﻿//used to qualify the type of a given datamap, for especial circumstances 
const DatamapType = "#datamaptype";

class DatamapConstants {

    static get DatamapType() {
        return DatamapType;
    }
}