﻿(function (angular) {
    "use strict";

    angular.module('sw_components', ["webcommons_services"]);

})(angular);