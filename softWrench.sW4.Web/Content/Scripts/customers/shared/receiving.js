﻿(function (angular) {
    "use strict";

angular.module("sw_layout").controller("ReceivingController", ReceivingController);
function ReceivingController($scope, contextService, alertService, searchService) {
    "ngInject";

    $scope.receive = function (compositionitem) {
        if (compositionitem['orderqty'] <= compositionitem['receivedqty']) {
            alertService.alert("Cannot receive more than you ordered !! Receipts Completed");
            return;
        }
        if (compositionitem['receivedqty'] == null) {
            //need to perform the query to get the total quantity due -- Extract everything into a function of poreceiving service later
            var qtydue = 0;
            const searchData = {
                ponum: compositionitem['ponum'],
                polinenum: String(compositionitem['polinenum'])
            };
            searchService.searchWithData("materialrecords", searchData).then(function (response) {
                const data = response.data;
                const resultObject = data.resultObject;
                var totalquantityreceived = 0;
                var i = 0;
                for (i = 0;i< resultObject.length;i++) {
                    totalquantityreceived = totalquantityreceived + resultObject[i]['quantity'];
                }
                qtydue = compositionitem['orderqty'] - totalquantityreceived;
                // prepopulate the values for the matrectrans record
                const clonedItem = {};
                angular.copy(compositionitem, clonedItem);
                const originalPoNum = clonedItem['ponum'];
                const originalPoLineNum = String(clonedItem['polinenum']);
                clonedItem['polinenum'] = originalPoLineNum;
                clonedItem['#qtydue'] = qtydue;
                $scope.$emit(JavascriptEventConstants.CompositionEdit, clonedItem);
            });
            //open a schema to submit a matrectrans record
        }
        return;
    };
}

window.ReceivingController = ReceivingController;

})(angular);