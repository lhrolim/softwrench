﻿namespace softwrench.sw4.dashboard.classes.service.statistics {
    public sealed class StatisticsConstants {
        public const string CONTEXT_FILTER_VARIABLE_NAME = "{statistics_context_filter}";
        public const string FIELD_VALUE_VARIABLE_NAME = "COUNTBY";
        public const string FIELD_LABEL_VARIABLE_NAME = "LABEL";
    }
}
