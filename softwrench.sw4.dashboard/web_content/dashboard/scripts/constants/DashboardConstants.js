﻿const finishLoading = "dash_finishloading";
const dashboardSaved = "dash_dashsaved";
const refreshPanel = "dash_refreshpanel";
const dashboardPanelAssociated = "dash_panelassociated";
const appFieldsLoaded = "app_fields_loaded";

class DashboardEventConstants {

    static get FinishLoading() {
        return finishLoading;
    }

    
    static get DashboardSaved() {
        return dashboardSaved;
    }

    static get PanelAssociated() {
        return dashboardPanelAssociated;
    }

    static get AppFieldsLoaded() {
        return appFieldsLoaded;
    }

    static get RefreshPanel() {
        return refreshPanel;
    }

   
}