﻿using cts.commons.simpleinjector;

namespace cts.commons.persistence {
    public interface IMaximoHibernateDAO : IBaseHibernateDAO, ISingletonComponent {
        // to mark only
    }
}