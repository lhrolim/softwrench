﻿namespace cts.commons.persistence {
    public interface IBaseEntity {
        int? Id { get; set; }
    }
}
